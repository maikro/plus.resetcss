const { series } = require('gulp')
const { clean } = require('./build/clean')
const { minifyToRpx, minifyToRem, minifyToPx } = require('./build/styleUtil')

function build(cd) {
	minifyToRpx()
	minifyToRem()
	minifyToPx()
	cd()
}

exports.clean = clean
exports.default = series(clean, build)
