const through2 = require('through2')

const unitTo = function(options = {}) {
    // const regExp = new RegExp(`(\d*\.?\d+)${options.unit}`, 'ig');
    // const regExp = new RegExp(`${options.unit}`, 'ig');

    return through2.obj(function(file, _, cb) {
        if (options.outputUnit && file.isBuffer()) {
        
			let str = file.contents.toString()

            str = pxAndRemInterturn({
                content: str,
                unit: options.unit,
                updateUnit: options.outputUnit
            })

            file.contents = Buffer.from(str)
        }
        cb(null, file);
    })
};

/* 
    px和rem单位互转
    content: 样式表的内容
    unit: 需要替换的单位名称
    updateUnit: 替换后的单位名称
    rule: 
        单位换算规则，多少rem = px || px = rem
        例如：当rule为100时即表示 1rem = 100px
*/
function pxAndRemInterturn({
    content,
    unit,
    rule,
    updateUnit
}) {
    rule = rule || 100
    if (!rule) return content;
    const reg = {
            px: /[\:]*[\s]*[-]*[\s]*[0-9]*[\.]*[0-9]*\p\x/g,
            rem: /[\:]*[\s]*[-]*[\s]*[0-9]*[\.]*[0-9]*rem/g,
            number: /[0-9]*[\.]*[0-9]*/g
        }
        // 获取到所有匹配的内容
    const res = content.match(reg[unit])
    
        // 传进来的是正则匹配的结构内容
    function getRightData(arr) {
        let result = []
        for (let i = 0; i < arr.length; i++) {
            result = result.concat(arr[i].match(/\d+/g))
        }
        return result
    }
    const rightData = getRightData(res)
    
        // 单位换算
    function unitConversion({
        num,
        unit,
        updateUnit,
        rule
    }) {
        let res = ''
        if (unit === 'px' && updateUnit === 'rem') {
            res = num / rule
        } else if (unit === 'rem' && updateUnit === 'px') {
            res = num * rule
        } else {
            res = num
        }
        return res
    }

    for (let i = 0; i < rightData.length; i++) {

        let txt = unitConversion({
                num: rightData[i],
                unit,
                updateUnit,
                rule
            })
        content = content.replace(res[i], res[i].replace(rightData[i], txt).replace(unit, updateUnit))
    }
    return content
}

module.exports = unitTo;