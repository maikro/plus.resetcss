const del = require('del')

function clean(cd) {
	const deletePaths = del.sync(['dist/*'])
	console.log(`删除文件和目录: ${deletePaths.join('\n')}`)
	cd()
}

exports.clean = clean
