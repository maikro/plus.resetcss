const { src, dest, series } = require('gulp'),
	sass = require('gulp-sass')(require('dart-sass')),
	cleanCSS = require('gulp-clean-css'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	unitTo = require('./unitTo');

const unit = 'px'
const outputPath = './dist'
const outputNamePrefix = 'plus.resetcss'

/*
* px单位
* */
function minifyToPx() {
	return src('./src/**/*.scss')
		.pipe(concat(`${outputNamePrefix}.px.scss`))
		.pipe(unitTo({ unit, outputUnit: 'px' }))
		.pipe(dest(outputPath))	// 单位转换并导出
		.pipe(sass().on('error', sass.logError)) // sass换成css文件
		.pipe(autoprefixer()) // 添加各大浏览器兼容前缀
		.pipe(dest(outputPath))
		.pipe(rename({ suffix: '.min' }))
		.pipe(cleanCSS()) // 压缩css文件，删除空格换行等
		.pipe(dest(outputPath))
}

/*
* rpx
* */
function minifyToRpx() {
	return src('./src/**/*.scss')
		.pipe(concat(`${outputNamePrefix}.rpx.scss`))
		.pipe(unitTo({ unit, outputUnit: 'rpx' }))
		.pipe(dest(outputPath))
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(dest(outputPath))
		.pipe(rename({ suffix: '.min' }))
		.pipe(cleanCSS())
		.pipe(dest(outputPath))
}

/*
* rem
* */
function minifyToRem() {
	return src('./src/**/*.scss')
		.pipe(concat(`${outputNamePrefix}.rem.scss`))
		.pipe(unitTo({ unit, outputUnit: 'rem' }))
		.pipe(dest(outputPath))
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(dest(outputPath))
		.pipe(rename({ suffix: '.min' }))
		.pipe(cleanCSS())
		.pipe(dest(outputPath))
}

exports.minifyToPx = minifyToPx
exports.minifyToRpx = minifyToRpx
exports.minifyToRem = minifyToRem
