# 常用css样式库

##以下为在uniapp中的使用方法，其它项目大同小异

## 安装

```shell
npm install plus.resetcss
// 或
yarn add plus.resetcss
```

## 使用

在App.vue项目入口文件引入

```javascript
// App.vue
<style lang="scss">
    ...其它css文件
    @import "@/styles/index.scss";
</style>
```

非uniapp项目中可打开 <a href="https://gitee.com/maikro/plus.resetcss/raw/master/dist/plus.resetcss.rpx.min.css" target="_blank">plus.resetcss.rpx.min.css</a> 复制到您的文件中以标签方式引入使用
```html
<link rel="stylesheet" href="plus.resetcss.rpx.min.css">
```


## 说明

- [reset 初始化默认样式](#reset)
- [flex 弹性布局](#flex)
- [width 宽度](#width)
- [height 高度](#height)
- [text 文字处理](#text)
- [space 内边距和外边距的简写](#space)


## <span id="reset">reset 初始化默认样式</span>

这里只做了简单的初始化样式

```css
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

a {
    text-decoration: none;
}
a:hover {
    opacity: .9;
}
```

## <span id="flex">flex 弹性布局</span>

| class | 描述 |
|-|-|
| -flex | 变为弹性盒子 |
| -inline-flex | 变为行内弹性盒子 |
| -flex-col | 垂直的弹性盒子 |
| -flex-col-reverse | flex-direction: column-reverse |
| -flex-row | 垂直的弹性盒子 |
| -flex-row-reverse | flex-direction: row-reverse |
| -flex-center | 垂直水平居中 |
| -flex-wrap | 让弹性盒元素在必要的时候拆行 |
| -flex-nowrap | 不拆行或不拆列 |
| -flex-wrap-reverse | flex-wrap: wrap-reverse |
| -flex-1 | flex: 1 1 0%; |
| -flex-auto | flex: 1 1 auto; |
| -flex-initial | flex: 0 1 auto; |
| -flex-none | flex: none; |
| -flex-grow | flex-grow: 1; |
| -flex-grow-0 | flex-grow: 0; |
| -flex-y-start | justify-content: flex-start; |
| -flex-y-end | justify-content: flex-end; |
| -flex-y-center | justify-content: center; |
| -flex-y-between | justify-content: space-between; |
| -flex-y-around | justify-content: space-around; |
| -flex-y-evenly | justify-content: space-evenly; |
| -flex-x-start | align-items: flex-start; |
| -flex-x-end |  align-items: flex-end; |
| -flex-x-center | align-items: center; |
| -flex-x-baseline | align-items: baseline; |
| -flex-x-stretch | align-items: stretch; |
| -flex-xy-center | 垂直水平居中 |

## <span id="width">width 宽度</span>

| class | 描述 |
|-|-|
| -w-0 | width: 0; |
| -w-auto | width: auto; |
| -w-1_2 | width: 50%; |
| -w-1_3 |  width: 33.333%; |
| -w-1_4 | width: 25%; |
| -w-1_4 | width: 25%; |
| -w-1_5 | width: 20%; |
| -w-full | width: 100%; |
| -w-screen | width: 100vw; |
| -w-min | width: min-content; |
| -w-max | width: max-content; |
| -min-w-0 | min-width: 0px; |
| -min-w-full | min-width: 100%; |
| -min-w-screen | min-width: 100vw; |
| -min-w-min | min-width: min-content; |
| -min-w-max | min-width: max-content; |
| -max-w-0 | max-width: 0; |
| -max-w-none | max-width: none; |
| -max-w-full | max-width: 100%; |
| -max-w-screen | max-width: 100vw; |


## <span id="height">height 高度</span>

| class | 描述 |
|-|-|
| -h-0 | height: 0; |
| -h-auto | height: auto; |
| -h-1_2 | height: 50%; |
| -h-1_3 |  height: 33.333%; |
| -h-1_4 | height: 25%; |
| -h-1_4 | height: 25%; |
| -h-1_5 | height: 20%; |
| -h-full | height: 100%; |
| -h-screen | height: 100vw; |
| -h-min | height: min-content; |
| -h-max | height: max-content; |
| -min-h-0 | min-height: 0px; |
| -min-h-full | min-height: 100%; |
| -min-h-screen | min-height: 100vw; |
| -min-h-min | min-height: min-content; |
| -min-h-max | min-height: max-content; |
| -max-h-0 | max-height: 0; |
| -max-h-none | max-height: none; |
| -max-h-full | max-height: 100%; |
| -max-h-screen | max-height: 100vw; |


## <span id="text">text 文字处理</span>

| class | 描述 |
|-|-|
| -font-bold | 字体加粗 |
| -font-normal | 默认，标准的字体 |
| -text-left | 文本左对齐 |
| -text-center | 文本居中 |
| -text-right | 文本右对齐 |
| -text-justify | 两边对齐 |
| -line-through | 定义穿过文本下的一条线 |
| -underline | 定义文本下的一条线 |
| -no-underline | 默认。定义标准的文本 |
| -line-\[1-5\] | 文字溢出显示省略号，范围从1到5，列：-line-2，超过两行其余的隐藏并显示省略号 |


## <span id="space">space 内边距和外边距的简写</span>

> <p style="color: red">定义内外边距,范围从0-80的双数和能被5除尽的数</p>

| class | 描述 |
|-|-|
| -m-\[0-80\] | 0-80范围四条边的外边距，-m同等与margin属性，列：-m-10等于margin: 10rpx |
| -mx-\[0-80\] | 0-80左右两边的外边距，-mx同等与margin-left跟margin-right属性，列：-mx-10等于margin-left: 10rpx, margin-right:10rpx |
| -py-\[0-80\] | 0-80上下两边的外边距，-my同等与margin-top跟margin-bottom属性，列：-my-10等于margin-top: 10rpx, margin-bottom:10rpx |
| -m\[l\/t\/r\/b\]-\[0-80\] | 单边的外边距，l/t/r/b是left,top,right,bottom的简写,列：-ml-10等于margin-left: 10rpx |
| -m\[l\/t\/r\/b\]-auto | 单边的外边距，l/t/r/b是left,top,right,bottom的简写,列：-ml-auto等于margin-left: auto |
| -p-\[0-80\] | 0-80范围四条边的内边距，-p同等与padding属性，列：-p-10等于padding: 10rpx |
| -px-\[0-80\] | 0-80左右两边的内边距，-px同等与padding-left跟padding-right属性，列：-px-10等于padding-left: 10rpx, padding-right:10rpx |
| -py-\[0-80\] | 0-80上下两边的内边距，-py同等与padding-top跟padding-bottom属性，列：-my-10等于padding-top: 10rpx, padding-bottom:10rpx |
| -p\[l\/t\/r\/b\]-\[0-80\] | 单边的内边距，l/t/r/b是left,top,right,bottom的简写,列：-pl-10等于padding-left: 10rpx |
